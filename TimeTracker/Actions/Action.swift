//
//  Action.swift
//  TimeTracker
//
//  Created by Franz Murner on 12.10.21.
//

import Foundation

protocol Action{
    
    func runAction()
    
}
