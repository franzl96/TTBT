//
//  AddTaskEntry.swift
//  TimeTracker
//
//  Created by Franz Murner on 12.10.21.
//

import Foundation

class AddTaskEntry: Action{
    
    func runAction() {
        ConsoleHelper.clearView()
        ConsoleHelper.printHeader()
        ConsoleHelper.printViewHeader(message: Messages.neueAufgabe.rawValue)
        ConsoleHelper.printFF("")

        let dimensions = [1, 15, 20, 20, 70, 15]
        
        HeaderLine(dimensions: dimensions)
            .addColumn(with: "TimeIn")
            .addColumn(with: "Ticket-Nr")
            .addColumn(with: "Name")
            .addColumn(with: "Beschreibung")
            .addColumn(with: "TimeOut")
            .draw()
        
        ContentLine(dimensions: dimensions)
            .addColumn(with: "")
            .addColumn(with: "")
            .addColumn(with: "")
            .addColumn(with: "")
            .addColumn(with: "")
            .draw()
        
        ConsoleHelper.printFF("")
        
        ConsoleHelper.printOptionsLine(option: "-c-", optionMessage: "aktuelle Zeit eintragen")
        ConsoleHelper.printOptionsLine(option: "-q-", optionMessage: "Bearbeitung abbrechen und zurück zum Hauptmenü")
        ConsoleHelper.printOptionsLine(option: "Eingabe + Enter", optionMessage: "Fortfahren...")

        ConsoleHelper.printFF("")
        ConsoleHelper.printFF("")
        ConsoleHelper.printFF("", linebreak: false)


        ANSICommands.setCursor(relativeUp: 6)
        
        var currentRelativeRight = 0
        
        var results = [String]()
        
        for i in 0..<dimensions.count - 1{
            currentRelativeRight += dimensions[i]
            
            if i == 0 || i == dimensions.count - 2{
                var date: String?
                
                var firstTry = true
                
                repeat{
                    if !firstTry{
                        ANSICommands.setCursor(relativeDown: 4)
                        ANSICommands.setCursorToStartOfLine()
                        ConsoleHelper.printFF("Falsches Zeitformat! Bitte h:m oder h:m:s oder -c- für aktuelle Zeit!", with: .red, bold: true, linebreak: false)
                        ANSICommands.setCursor(relativeUp: 4)
                    }
                    
                    firstTry = false
                    
                    ANSICommands.setCursorToStartOfLine()
                    ANSICommands.setCursor(relativeUp: 1)
                    ANSICommands.setCursorColumnTo(currentRelativeRight)
                    ConsoleHelper.printFF(String(repeating: " ", count: 12), linebreak: false)
                    ANSICommands.setCursorColumnTo(currentRelativeRight)

                    let input = readLine() ?? ""
                    
                    date = DateAndTimeService.service.getTimeStringFrom(userInput: input)
                    
                    if(input == "-q-") {
                        goDown(isQuit: true)
                        return
                    }else if (input == "-c-"){
                        date = DateAndTimeService.service.currentTimeString()
                        ANSICommands.setCursor(relativeUp: 1)
                        ANSICommands.setCursorColumnTo(currentRelativeRight)
                        ConsoleHelper.printFF(date ?? "", linebreak: false)
                    }else if (date != nil){
                        ANSICommands.setCursor(relativeUp: 1)
                    }
                    
                }while date == nil
                
                results.append(date ?? "")
                
                ANSICommands.setCursor(relativeDown: 5)
                ANSICommands.setCursorToStartOfLine()
                ANSICommands.clearCurrentLine()
                ANSICommands.setCursor(relativeUp: 4)
                
            }else{
                
                ANSICommands.setCursor(relativeUp: 1)
                ANSICommands.setCursorColumnTo(currentRelativeRight)
                
                var input = readLine() ?? ""
                
                if(input == "-q-") {
                    goDown(isQuit: true)
                    return
                }else if (input == "-c-"){
                    input = DateAndTimeService.service.currentTimeString()
                    ANSICommands.setCursor(relativeUp: 1)
                    ANSICommands.setCursorColumnTo(currentRelativeRight)
                    ConsoleHelper.printFF(input, linebreak: false)
                    ANSICommands.setCursor(relativeDown: 1)
                }
                
                results.append(input)
                
            }
        }
        
        saveData(results: results)
        goDown(isQuit: false)
    }
    
    private func goDown(isQuit: Bool){
        for _ in 1...4{
            ANSICommands.setCursor(relativeDown: 1)
            ANSICommands.setCursorToStartOfLine()
            ANSICommands.clearCurrentLine()
        }
        
        ANSICommands.setCursor(relativeUp: 3)
        
        if isQuit{
            ConsoleHelper.printFF("Vorgang abgebrochen! -> Weiter mit Enter!", with: .red, bold: true, linebreak: false)
        }else{
            ConsoleHelper.printFF("Daten gespeichert! -> Weiter mit Enter!", with: .green, bold: true, linebreak: false)
        }
        
        _ = ConsoleHelper.readLineFF(linebreak: false)
        
        ANSICommands.setCursor(relativeDown: 4)

        ConsoleHelper.clearView()
    }
    
    private func saveData(results: [String]){
        let taskEntry = TaskEntry(taskId: 0,
                                  timeInString: results[0],
                                  timeOutString: results[4],
                                  name: results[2],
                                  ticketNr: results[1],
                                  description: results[3],
                                  completed: true)
        
        DayEntryRepository.service.addTaskEntryToDate(date: DateAndTimeService.service.currentDateString(), taskEntry: taskEntry)
    }
    
}

