//
//  QuitTask.swift
//  TimeTracker
//
//  Created by Franz Murner on 17.10.21.
//

import Foundation

class QuitTask: Action{
    
    func runAction() {
        ConsoleHelper.clearView()
        exit(0)
    }
    
}
