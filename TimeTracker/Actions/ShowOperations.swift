//
//  ShowOperations.swift
//  TimeTracker
//
//  Created by Franz Murner on 16.10.21.
//

import Foundation

class ShowOperations{
    
    static let dimensions = [1, 8, 20, 20, 15, 15, 15, 70]

    
    public static func printTableHeader(){
        HeaderLine(dimensions: dimensions)
            .addColumn(with: "Nr")
            .addColumn(with: "Ticket-Nr")
            .addColumn(with: "Name")
            .addColumn(with: "TimeIn")
            .addColumn(with: "TimeOut")
            .addColumn(with: "Summe")
            .addColumn(with: "Beschreibung")
            .draw()
    }
    
    public static func printTableContent(taskEntries: [TaskEntry]?)->Bool{
        guard let content = taskEntries,
              !content.isEmpty
        else {
            ConsoleHelper.printFF("Keine Einträge für heute vorhanden!", with: .cyan, bold: true, linebreak: true)
            return false
        }
        
        for entry in content{
            
            let durationInSec = DateAndTimeService.service.calculateDivBetween(timeIn: entry.timeInString, timeOut: entry.timeOutString)
            let timeString = DateAndTimeService.service.getTimeStringHMS(seconds: durationInSec)
            
            ContentLine(dimensions: dimensions)
                .addColumn(with: String(entry.taskId))
                .addColumn(with: entry.ticketNr)
                .addColumn(with: entry.name)
                .addColumn(with: entry.timeInString)
                .addColumn(with: entry.timeOutString)
                .addColumn(with: timeString)
                .addColumn(with: entry.description)
                .draw()
        }
        
        return true
    }
    
    
    
    
    
}
