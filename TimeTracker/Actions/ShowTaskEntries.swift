//
//  ShowTaskEntries.swift
//  TimeTracker
//
//  Created by Franz Murner on 12.10.21.
//

import Foundation

class ShowTaskEntries: Action{
    
    func runAction() {
        while(true){
            let taskEntries = DayEntryRepository.service.getTaskEntriesFromDate(date: DateAndTimeService.service.currentDateString())
            
            ConsoleHelper.clearView()
            ConsoleHelper.printHeader()
            ConsoleHelper.printViewHeader(message: Messages.ansehenDerTagesobjekte.rawValue)
            ConsoleHelper.printFF("")
            ShowOperations.printTableHeader()
            let dataAvailable = ShowOperations.printTableContent(taskEntries: taskEntries)
            ConsoleHelper.printFF("")
            if dataAvailable{
                ConsoleHelper.printOptionsLine(option: "-d-", optionMessage: "Eintrag Löschen")
                ConsoleHelper.printOptionsLine(option: "Enter", optionMessage: "Zurück zum Hauptmenü")
                if !checkInput() { break }
            }else{
                ConsoleHelper.printOptionsLine(option: "Enter", optionMessage: "Zurück zum Hauptmenü")
                _ = ConsoleHelper.readLineFF(linebreak: true)
                break
            }

        }
        
        ConsoleHelper.clearView()
    }
    
    func checkInput()->Bool{
        guard let input = ConsoleHelper.readLineFF(),
              input == "-d-" else {
                  return false
              }
        
        ANSICommands.clearLines(lines: 3)
        
        var result = false
        
        repeat{
            
            ConsoleHelper.printFF("Gewünschter Eintrag oder -q- für Abbrechen: ", with: .yellow, linebreak: false)
            
            guard let delInput = ConsoleHelper.readLineFF(linebreak: false),
                  delInput != "-q-"
            else {
                break
            }
            
            
            guard let id = Int(delInput) else {
                ANSICommands.setCursor(relativeDown: 1)
                ANSICommands.clearCurrentLine()
                ConsoleHelper.printFF("Keine ID eingegeben!", with: .red, bold: true, linebreak: false)
                result = false
                ANSICommands.setCursor(relativeUp: 2)
                ANSICommands.setCursorColumnTo(0)
                ANSICommands.clearCurrentLine()
                continue
            }
            
            let deleted = DayEntryRepository.service.deleteTaskEntry(date: DateAndTimeService.service.currentDateString(), taskEntryID: id)
            
            guard deleted == true else {
                ANSICommands.setCursor(relativeDown: 1)
                ANSICommands.clearCurrentLine()
                ConsoleHelper.printFF("ID ist nicht in Datenbank!", with: .red, bold: true, linebreak: false)
                result = false
                ANSICommands.setCursor(relativeUp: 2)
                ANSICommands.setCursorColumnTo(0)
                ANSICommands.clearCurrentLine()
                continue
            }
            
            
            result = true
            
        }while(!result)
        
        ANSICommands.setCursor(relativeDown: 2)
        
        return true
        
    }
}
