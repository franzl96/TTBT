//
//  ShowTimePerTicket.swift
//  TimeTracker
//
//  Created by Franz Murner on 16.10.21.
//

import Foundation

class ShowTimePerTicket: Action{
    
    func runAction() {
        ConsoleHelper.clearView()
        ConsoleHelper.printHeader()
        ConsoleHelper.printViewHeader(message: Messages.ansehenDerTagesobjekte.rawValue)
        ConsoleHelper.printFF("")
        
        let taskEntries = DayEntryRepository.service.getTaskEntriesFromDate(date: DateAndTimeService.service.currentDateString())
        
        guard let taskEntries = taskEntries,
              !taskEntries.isEmpty
        else{
            ShowOperations.printTableHeader()
            ConsoleHelper.printFF("Keine Einträge für heute vorhanden!", with: .cyan, bold: true, linebreak: true)
            ConsoleHelper.printFF("")
            ConsoleHelper.printOptionsLine(option: "Enter", optionMessage: "Zurück zum Hauptmenü")
            _ = ConsoleHelper.readLineFF()
            ConsoleHelper.clearView()
            return
        }
        
        var taskEntriesByTicket = [String : [TaskEntry]]()
        
        
        for taskEntry in taskEntries{
            if(taskEntriesByTicket[taskEntry.ticketNr] == nil){
                taskEntriesByTicket[taskEntry.ticketNr] = [TaskEntry]()
            }
            
            taskEntriesByTicket[taskEntry.ticketNr]?.append(taskEntry)
        }
        
        for taskEntries in taskEntriesByTicket{
            var sum: TimeInterval = 0
            for taskEntry in taskEntries.value {
                sum += DateAndTimeService.service.calculateDivBetween(timeIn: taskEntry.timeInString, timeOut: taskEntry.timeOutString)
            }
            
            let durationString = DateAndTimeService.service.getTimeStringHMS(seconds: sum)
            
            ShowOperations.printTableHeader()
            _ = ShowOperations.printTableContent(taskEntries: taskEntries.value)
            ConsoleHelper.printFF("")
            ConsoleHelper.printFF("Die Tagessumme für das Ticket \(taskEntries.key) beträgt \(durationString)", with: .blue, bold: true)
            ConsoleHelper.printFF("")
        }
        
        
        ConsoleHelper.printOptionsLine(option: "Enter", optionMessage: "Zurück zum Hauptmenü")
        _ = ConsoleHelper.readLineFF()
        ConsoleHelper.clearView()
    }
}
