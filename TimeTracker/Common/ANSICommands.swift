//
//  ANSICommands.swift
//  TimeTracker
//
//  Created by Franz Murner on 10.10.21.
//

import Foundation

enum ANSICommands: String{
    
    case boldText = "\u{001B}[1m"
    case reset = "\u{001B}[22m"
    case clearView = "\u{001B}[2J"
    case goToHome = "\u{001B}[H"
    case clearLine = "\u{001B}[2K"
    
    case escapeSequence = "\u{001B}["


    static func setCursor(relativeRight: Int){
        print("\u{001B}[\(relativeRight)C", terminator: "")
    }
    
    static func setCursor(relativeUp: Int){
        print("\u{001B}[\(relativeUp)A", terminator: "")
    }
    
    static func setCursor(relativeDown: Int){
        print("\u{001B}[\(relativeDown)B", terminator: "")

    }
    
    static func setCursor(relativeLeft: Int){
        print("\u{001B}[\(relativeLeft)D", terminator: "")
    }
    
    static func setCursorToStartOfLine(){
        print("\u{001B}[0G", terminator: "")
    }
    
    static func setCursorColumnTo(_ line: Int){
        print("\u{001B}[\(line + 1)G", terminator: "")
    }
    
    static func clearLines(lines: Int){
        for _ in 1...lines{
            print(ANSICommands.clearLine.rawValue, terminator: "")
            setCursor(relativeUp: 1)
        }
    }
    
    static func clearCurrentLine(){
        print(ANSICommands.clearLine.rawValue, terminator: "")
    }
    
    
}
