//
//  ConsoleDimension.swift
//  TimeTracker
//
//  Created by Franz Murner on 17.10.21.
//

import Foundation

class ConsoleDimension{
    
    public var width = 0
    public var height = 0
    
    init(width: Int, height: Int){
        self.width = width
        self.height = height
    }
    
    
}
