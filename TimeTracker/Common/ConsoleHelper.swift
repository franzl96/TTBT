//
//  ConsoleHelper.swift
//  TimeTracker
//
//  Created by Franz Murner on 09.10.21.
//

import Foundation
import Darwin

class ConsoleHelper{
    
    
    
    static var printedLines = 0
    
    static func windowDimensions()->ConsoleDimension{
        let dim = ConsoleDimension(width: 0, height: 0)
        var w = winsize()
        if ioctl(STDOUT_FILENO, TIOCGWINSZ, &w) == 0 {
            dim.width = Int(w.ws_col)
            dim.height = Int(w.ws_row)
        }
        return dim
    }
    
    static func printHeader(){
        let header = createHeaderString(message: Messages.applicationHeader.rawValue, lineObject: "*", maxLength: windowDimensions().width)
        printFF(header, bold: true)
    }
    
    static func printViewHeader(message: String){
        let header = createHeaderString(message: message, lineObject: "-", maxLength: windowDimensions().width)
        printFF(header, with: .white, bold: true)
    }
    
    static func printOptionsLine(option: String, optionMessage: String){
        ConsoleHelper.printFF(option, with: .yellow, bold: true, linebreak: false)
        ConsoleHelper.printFF(": ", with: .white, bold: true, linebreak: false)
        ConsoleHelper.printFF(optionMessage)
    }
    
    
    static func clearScreen(){
        print(ANSICommands.clearView.rawValue)
        print(ANSICommands.goToHome.rawValue)
        printHeader()
    }
    
    static func createHeaderString(message: String, lineObject: String, maxLength: Int)->String{
        let minMessage = "- \(message) -"
        
        var difference = maxLength - minMessage.count

        
        if difference > 0{
            if difference % 2 == 1{
                difference -= 1
            }
            
            return String(repeating: "-", count: difference / 2) + minMessage + String(repeating: "-", count: difference / 2)
        }else{
            return minMessage

        }
    }
    

    static func printMainMenu(){
        

    }
    

    

    
    
    public static func getCurrentDayEntryList(dayEntries: [DayEntry])->DayEntry?{
        let dateString = DateAndTimeService.service.currentTimeString()
        
        guard let currentEntry = dayEntries.last,
              currentEntry.dateString == dateString
        else {
            return nil
        }
        
        return currentEntry
    }
    
    
    
   
    public static func printFF(_ items: Any..., with color: ANSIColors = .white, bold: Bool = false, linebreak: Bool = true){
        print(color.rawValue, terminator: "")
        if bold{
            print(ANSICommands.boldText.rawValue, terminator: "")
        }
        items.forEach { text in
            print(text, terminator: "")
        }
        print(ANSICommands.reset.rawValue, terminator: "")
        if(linebreak){
            print("", terminator: "\n")
            printedLines += 1
        }
        print(ANSIColors.white.rawValue, terminator: "")
        
    }
    
    public static func readLineFF(linebreak: Bool = true)->String?{
        if linebreak{
            printedLines += 1
        }
        return readLine()
    }
    
    public static func clearView(){
        guard printedLines > 0 else { return }
        ANSICommands.clearLines(lines: printedLines)
        printedLines = 0
    }
    
    public static func quitService(){
        printHeader()
        //print("Service wurde beendet!")
        exit(0)
    }
    
    
}
