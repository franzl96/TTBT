//
//  ViewHeaders.swift
//  TimeTracker
//
//  Created by Franz Murner on 17.10.21.
//

import Foundation

enum Messages: String{
    
    case neueAufgabe = "Neue Aufgabe anlegen!"
    case ansehenDerTagesobjekte = "Ansehen und Bearbeiten der Entries des heutigen Tages"
    case berechnungDerTicketzeiten = "Berechnen der Zeiten pro Ticket"
    case vorgangAbbrechen = "Abbrechen: Enter drücken"
    case vorgangBeenden = "Beenden: Enter drücken"
    case applicationHeader = "TimeTracker v. 1.0 - TapApp Software"

    
    
}
