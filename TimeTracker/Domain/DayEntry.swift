//
//  DateEntry.swift
//  TimeTracker
//
//  Created by Franz Murner on 08.10.21.
//

import Foundation

struct DayEntry: Codable{
    
    var highestTaskId: Int
    let dateString: String
    var taskEnties: [TaskEntry]
}
