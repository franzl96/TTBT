//
//  entry.swift
//  TimeTracker
//
//  Created by Franz Murner on 08.10.21.
//

import Foundation

struct TaskEntry: Codable{
    var taskId: Int
    let timeInString: String
    var timeOutString: String
    let name: String
    let ticketNr: String
    var description: String
    var completed: Bool
}
