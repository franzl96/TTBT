//
//  ContentLine.swift
//  TimeTracker
//
//  Created by Franz Murner on 12.10.21.
//

import Foundation

class ContentLine: Table{
    
    private var dimensions = [Int]()
    private var contentInformation = [String]()
    
    init(dimensions: [Int]){
        self.dimensions = dimensions
    }
    
    init(){
        self.dimensions.append(1)
    }
    
    func addColumn(with content: String, dimension: Int)->Table{
        contentInformation.append(content)
        dimensions.append(dimension)
        
        return self
    }
    
    func addColumn(with content: String)->Table{
        contentInformation.append(content)
        
        return self
    }
    
    func draw(){
        var contentLine = String(repeating: " ", count: dimensions[0])
        
        dimensions.removeFirst()
        
        guard contentInformation.count == dimensions.count else {
            print("Dimension count and column count not equal!!")
            return
        }
        
        for i in 0..<contentInformation.count{
            contentLine += paddingString(length: dimensions[i] - 3 , str: contentInformation[i])
            
            
            if i < contentInformation.count - 1{
                contentLine += " | "
            }
        }
        
        ConsoleHelper.printFF(contentLine)

    }
    
    public func paddingString(length: Int, str: String)->String{
        return str.padding(toLength: length, withPad: " ", startingAt: 0)
    }
    
}
