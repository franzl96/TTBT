//
//  DrawTable.swift
//  TimeTracker
//
//  Created by Franz Murner on 12.10.21.
//

import Foundation

class HeaderLine: Table{
    
    private var dimensions = [Int]()
    private var headerInformation = [String]()
    
    init(dimensions: [Int]){
        self.dimensions = dimensions
    }
    
    init(){
        self.dimensions.append(1)
    }
    
    
    
    func addColumn(with content: String, dimension: Int)->Table{
        headerInformation.append(content)
        dimensions.append(dimension)
        
        return self
    }
    
    func addColumn(with content: String)->Table{
        headerInformation.append(content)
        
        return self
    }
    
    func draw(){
        var topLine = String(repeating: "-", count: dimensions[0])
        var headerInformationSection = String(repeating: " ", count: dimensions[0])
        var bottomLine = String(repeating: "-", count: dimensions[0])
        
        dimensions.removeFirst()
        
        guard headerInformation.count == dimensions.count else {
            print("Dimension count and column count not equal!!")
            return
        }
        
        
        for i in 0..<headerInformation.count{
            topLine += String(repeating: "-", count: dimensions[i] - 3)
            headerInformationSection += paddingString(length: dimensions[i] - 3 , str: headerInformation[i])
            bottomLine += String(repeating: "-", count: dimensions[i] - 3)
            

            
            if i < headerInformation.count - 1{
                topLine += "---"
                headerInformationSection += " | "
                bottomLine += "-+-"
            }
        }
        
        ConsoleHelper.printFF(topLine)
        ConsoleHelper.printFF(headerInformationSection)
        ConsoleHelper.printFF(bottomLine)
        
    }
    
    public  func paddingString(length: Int, str: String)->String{
        return str.padding(toLength: length, withPad: " ", startingAt: 0)
    }
    
}
