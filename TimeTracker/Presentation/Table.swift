//
//  Table.swift
//  TimeTracker
//
//  Created by Franz Murner on 12.10.21.
//

import Foundation

protocol Table{
    
    
    func addColumn(with content:String, dimension: Int)->Table
    
    func addColumn(with content:String)->Table


    
    func draw()
    
    
    
}
