//
//  main.swift
//  TimeTracker
//
//  Created by Franz Murner on 08.10.21.
//

import Foundation

let menuActions: [String : Action] = [
    "1": AddTaskEntry(),
    "2": ShowTaskEntries(),
    "3": ShowTimePerTicket(),
    "q": QuitTask()
]

while(true){
    
    ConsoleHelper.printHeader()
    ConsoleHelper.printViewHeader(message: "Hauptmenü")
    ConsoleHelper.printOptionsLine(option: "1", optionMessage: Messages.neueAufgabe.rawValue)
    ConsoleHelper.printOptionsLine(option: "2", optionMessage: Messages.ansehenDerTagesobjekte.rawValue)
    ConsoleHelper.printOptionsLine(option: "3", optionMessage: Messages.berechnungDerTicketzeiten.rawValue)
    ConsoleHelper.printOptionsLine(option: "q", optionMessage: "Anwendung Beenden")

    let value = ConsoleHelper.readLineFF()

    guard let action = menuActions[value ?? ""] else {
        ConsoleHelper.clearView()
        continue
    }
    
    action.runAction()
}
