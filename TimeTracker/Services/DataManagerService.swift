//
//  DataManagerService.swift
//  TimeTracker
//
//  Created by Franz Murner on 08.10.21.
//

import Foundation

class DataManagerService{
    
    var url = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first

    static let service = DataManagerService()
    
    let jsonEncoder = JSONEncoder()
    let jsonDecoder = JSONDecoder()
    
    
    private init(){
        jsonEncoder.outputFormatting = .prettyPrinted
        url?.appendPathComponent("timeData")
        url?.appendPathExtension("json")
    }
    
    
    func getDataFromFileSystem()->[DayEntry]{
        guard let url = url else {
            print("URL error")
            exit(1)
        }
        
        do{
            let _ = try url.checkPromisedItemIsReachable()
        }catch{
            saveDataToFileSystem(dayEntry: [DayEntry]())
        }
        
        do{
            return try jsonDecoder.decode([DayEntry].self, from: Data(contentsOf: url))
        }catch{
            print("Error while decoding data")
            exit(1)
        }
    }
    
    func saveDataToFileSystem(dayEntry: [DayEntry]){
        guard let url = url else {
            print("URL error")
            exit(1)
        }
        
        do{
            let data = try jsonEncoder.encode(dayEntry)
            try data.write(to: url)
        }catch{
            print("Write or encoding error")
            exit(1)
        }
    }
    
    
    

    
}
