//
//  DayEntryRepository.swift
//  TimeTracker
//
//  Created by Franz Murner on 10.10.21.
//

import Foundation

class DayEntryRepository{
    
    private var dayEntryList: [DayEntry] = [DayEntry]()
    
    public static var service = DayEntryRepository()
    
    private init(){
        reloadDatabase()
    }
    
    private func reloadDatabase(){
        dayEntryList = DataManagerService.service.getDataFromFileSystem()
    }
    
    public func save(){
        DataManagerService.service.saveDataToFileSystem(dayEntry: dayEntryList)
    }
    
    private func searchDateEntry(date: String)->Int?{
        for i in 0..<dayEntryList.count{
            if dayEntryList[i].dateString == date{
                return i
            }
        }
        
        return nil
    }
    
    private func searchTaskEntry(id: Int, taskEntries: [TaskEntry]) ->Int?{
        for i in 0..<taskEntries.count{
            if taskEntries[i].taskId == id{
                return i
            }
        }
        
        return nil
    }
    
//    MARK: Repository Request Methods
    
    public func getTaskEntryFrom(date: String, id: Int)->TaskEntry?{
        guard let entries = getTaskEntriesFromDate(date: date),
              let index = searchTaskEntry(id: id, taskEntries: entries)
        else {return nil}
        
        return entries[index]
    }
    
    public func getTaskEntriesFromDate(date: String)->[TaskEntry]?{
        reloadDatabase()
        
        guard let index = searchDateEntry(date: date) else {
            return nil
        }
        
        return dayEntryList[index].taskEnties
    }
    
    public func updateTaskEntryToDate(date: String, taskEntry: TaskEntry){
        
        guard let taskEntries =  getTaskEntriesFromDate(date: date),
              let dateIndex = searchDateEntry(date: date),
              let taskIdIndex = searchTaskEntry(id: taskEntry.taskId, taskEntries: taskEntries)
        else { return }
        
        dayEntryList[dateIndex].taskEnties[taskIdIndex] = taskEntry

        
    }
    
    public func deleteTaskEntry(date: String, taskEntryID: Int)->Bool{
        
        guard let taskEntries =  getTaskEntriesFromDate(date: date),
              let dateIndex = searchDateEntry(date: date),
              let taskIdIndex = searchTaskEntry(id: taskEntryID, taskEntries: taskEntries)
        else { return false }
        
        dayEntryList[dateIndex].taskEnties.remove(at: taskIdIndex)
        
        save()
        
        return true
    }
    
    public func addTaskEntryToDate(date: String, taskEntry: TaskEntry){
        reloadDatabase()
        
        var taskEntry = taskEntry
        
        if let index = searchDateEntry(date: date){
            dayEntryList[index].highestTaskId += 1
            taskEntry.taskId = dayEntryList[index].highestTaskId
            dayEntryList[index].taskEnties.append(taskEntry)
        }else{
            taskEntry.taskId = 0
            dayEntryList.append(DayEntry(highestTaskId: 0,dateString: date, taskEnties: [taskEntry]))
        }
        save()
    }
    
    public func getOpenTickets(from date: String)->[TaskEntry]?{
        reloadDatabase()
        
        guard let index = searchDateEntry(date: date) else { return nil }
        
        var entries = [TaskEntry]()
        
        for entry in dayEntryList[index].taskEnties{
            if !entry.completed{
                entries.append(entry)
            }
        }
        
        return entries.isEmpty ? nil : entries
        
    }
    


    

    
    
    
    
    
    
    
    
}
