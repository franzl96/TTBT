//
//  Helper.swift
//  TimeTracker
//
//  Created by Franz Murner on 09.10.21.
//

import Foundation


class DateAndTimeService{
    
    private var dateFormatter = DateFormatter()
    private var timeFormatter = DateFormatter()
    private let userTimeFormatterMed = DateFormatter()
    private var userTimeFormatterShort = DateFormatter()


    public static let service = DateAndTimeService()
    
    private init(){
        dateFormatter.dateStyle = .short
        dateFormatter.timeStyle = .none
        dateFormatter.locale = .autoupdatingCurrent
        
        timeFormatter.dateStyle = .none
        timeFormatter.timeStyle = .medium
        timeFormatter.locale = .autoupdatingCurrent
        
        userTimeFormatterMed.dateStyle = .none
        userTimeFormatterMed.timeStyle = .medium
        userTimeFormatterMed.locale = .autoupdatingCurrent
        
        userTimeFormatterShort.dateStyle = .none
        userTimeFormatterShort.timeStyle = .short
        userTimeFormatterShort.locale = .autoupdatingCurrent
    }
    
    public func toDateStamp(dateString: String)->Date?{
        return dateFormatter.date(from: dateString)
    }
    
    public func toTimeStamp(timeString: String)->Date?{
        return timeFormatter.date(from: timeString)
    }
    
    public func toDateStampString(date: Date)->String{
        return dateFormatter.string(from: date)
    }
    
    public func toTimeString(time: Date)->String{
        return timeFormatter.string(from: time)
    }
    
    public func currentTimeString()->String{
        return timeFormatter.string(from: Date())
    }
    
    public func currentDateString()->String{
        return dateFormatter.string(from: Date())
    }
    
    
    
    public func getTimeStringFrom(userInput: String)->String?{
        
        var date = userTimeFormatterMed.date(from: userInput)
        if(date == nil){
            date = userTimeFormatterShort.date(from: userInput)
        }
        
        guard let date = date else { return nil }
        
        return toTimeString(time: date)
    }
    
    public func prepareDateForTableOutput(dateString: String)->String{
        guard let date = timeFormatter.date(from: dateString) else { return "?"}
        
        return userTimeFormatterMed.string(from: date)
    }
    
    public func getTimeStringHMS(seconds: TimeInterval)->String{
        var remaining = seconds
        let seconds = remaining.truncatingRemainder(dividingBy: 60)
        remaining = (remaining - seconds) / 60
        let minutes = remaining.truncatingRemainder(dividingBy: 60)
        remaining = (remaining - minutes) / 60
        let hours = remaining
        
        return String(format: "%.0fh %.0fm %.0fs", hours, minutes, seconds)
        
        
    }
    
    public func calculateDivBetween(timeIn: String, timeOut: String)->TimeInterval{
        guard let timeInStamp = toTimeStamp(timeString: timeIn),
              let timeOutStamp = toTimeStamp(timeString: timeOut)
        else {return 0}
        
        
        return timeOutStamp.timeIntervalSince(timeInStamp)
    }
    
    
    
//    12341s
    
    
    
    
}


