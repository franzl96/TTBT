//
//  TimeTrackeTests.swift
//  TimeTrackeTests
//
//  Created by Franz Murner on 16.10.21.
//

import XCTest

class TimeTrackerTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        
        var val = DateAndTimeService.service.getTimeStringHMS(seconds: 60)
        XCTAssert(val == "0h 1m 0s")
        
        val = DateAndTimeService.service.getTimeStringHMS(seconds: 3600)
        XCTAssert(val == "1h 0m 0s")
        
        val = DateAndTimeService.service.getTimeStringHMS(seconds: 3661)
        XCTAssert(val == "1h 1m 1s")

        
        
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        measure {
            // Put the code you want to measure the time of here.
        }
    }

}
